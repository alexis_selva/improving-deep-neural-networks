These are the programming assignments relative to Deep Learning - Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization (2nd part):

- Assignment 5.1: Initialization
- Assignment 5.2: Regularization
- Assignment 5.3: Gradient checking
- Assignment 6.1: Optimization 
- Assignment 6.2: Tensor flow

For more information, I invite you to have a look at https://www.coursera.org/learn/deep-neural-network
